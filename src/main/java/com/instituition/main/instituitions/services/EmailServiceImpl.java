package com.instituition.main.instituitions.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import com.instituition.main.instituitions.components.EmailService;

@Component
public class EmailServiceImpl implements EmailService {

	@Autowired
    public JavaMailSender emailSender;
	
		@Override
		public void sendMimeMessage(String userId, String password, String serviceUrl, String from, String[] to,
				String[] cc, String[] bcc, String subject, String body) throws Exception {
			
			SimpleMailMessage message = new SimpleMailMessage(); 
	        message.setTo(to); 
	        message.setSubject(subject); 
	        message.setText(body);
	        emailSender.send(message);
		}
		
		@Override
		public void sendSimpleMessage( String to, String subject, String text) {
			   
			        SimpleMailMessage message = new SimpleMailMessage(); 
			        message.setTo(to); 
			        message.setSubject(subject); 
			        message.setText(text);
			        emailSender.send(message);
			  
			    }
}
