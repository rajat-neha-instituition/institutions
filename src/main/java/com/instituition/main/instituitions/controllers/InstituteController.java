package com.instituition.main.instituitions.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.instituition.main.instituitions.EmailTemplates;
import com.instituition.main.instituitions.exception.ResourceNotFoundException;
import com.instituition.main.instituitions.model.Institute;
import com.instituition.main.instituitions.repository.InstituteRepository;
import com.instituition.main.instituitions.services.EmailServiceImpl;


@RestController

public class InstituteController {

	@Value("${mail.subject.mailverification}")
	private String mailVerificationSub;
	
	@Autowired
    private InstituteRepository instituteRepository;
	
	@Autowired
	private EmailServiceImpl emailService;
	
	@Autowired
	private EmailTemplates emailtemplate;
	

	@CrossOrigin("*")
	@PostMapping("/addingRequest")
    public Institute addNewRequest(@Valid @RequestBody Institute institute) {
		
		String instId = System.currentTimeMillis()/100000+"";	
		institute.setInstId(instId);
		institute.setStatus("New");
		emailService.sendSimpleMessage(institute.getEmail(), mailVerificationSub, emailtemplate.mailVerificationBody(instId) );
		Institute newRequest = instituteRepository.save(institute);
		return newRequest;
    }
	
	@RequestMapping(value="/veryfyingEmail", method=RequestMethod.GET)
    @ResponseBody
	public Institute verify(@RequestParam("instId") String instId) {
		
		Institute updated_inst = null;
		try {
			  Institute inst_obj = instituteRepository.findByInstId(instId);
			  if( inst_obj != null ) {
		 	  System.out.println("******* "+inst_obj.getAddress());
			  inst_obj.setStatus("Pending");
			  updated_inst = instituteRepository.save(inst_obj);
			}
		}
		catch (Exception e) {
			throw new ResourceNotFoundException("Institute not found");
		}
		 return updated_inst;
	   }
	
}
