package com.instituition.main.instituitions;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableJpaAuditing
@SpringBootApplication

public class InstituitionsApplication {

	public static void main(String[] args) {
		SpringApplication.run(InstituitionsApplication.class, args);
	}

}
