package com.instituition.main.instituitions;

import org.springframework.stereotype.Component;

@Component
public class EmailTemplates {
	
	public String mailVerificationBody(String instId) {
		return "please click here to verify your email address : http://localhost:8091/verifyEmail/?instId="+instId;
	} 

}
