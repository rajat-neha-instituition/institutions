package com.instituition.main.instituitions.components;

public interface EmailService {
	
	public void sendMimeMessage(String userId, String password, String serviceUrl, String from, String[] to,
			String[] cc,String[] bcc, String subject, String body) throws Exception;

	public void sendSimpleMessage( String to, String subject, String text) throws Exception;
}
