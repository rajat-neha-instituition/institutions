package com.instituition.main.instituitions.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.instituition.main.instituitions.model.Institute;

public interface InstituteRepository  extends JpaRepository<Institute, Long> {
	
	Institute findByInstId(String instituteId);
	
}
