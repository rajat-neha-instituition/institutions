package com.instituition.main.instituitions.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Table(name = "institute")
public class Institute extends AuditModel  {
	
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(generator = "institute_generator")
    @SequenceGenerator(
            name = "institute_generator",
            sequenceName = "institute_sequence",
            initialValue = 1000
    )
    private Long id;
	
	@NotNull
    @Size(min = 3, max = 100)
    private String title;

    @Column(columnDefinition = "text", name="address")
    private String address;
    
    @Column(columnDefinition = "text", name="pin_no")
    @Pattern(regexp="(^$|[0-9]{6})")
    private String pin_code;
    
    @Column( name="email")
    private String email; 

    @Column(unique= false,name="contact")
    @Pattern(regexp="(^$|[0-9]{10})")
    private String contact;
    
    @Column(unique= true, name="instid")
    @Pattern(regexp="(^$|[0-9]{8})")
    private String instId;
    
    @Column( name="password")
    @Pattern(regexp="(^$|[0-9,a-z,A-Z]{5,20})")
    private String password;

    @Column( name="status")
    private String status;
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPin_no() {
		return pin_code;
	}

	public void setPin_no(String pincode) {
		this.pin_code = pincode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}


	public String getInstId() {
		return instId;
	}

	public void setInstId(String instId) {
		this.instId = instId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
