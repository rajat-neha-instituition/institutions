import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) { }


  callHttp(url, method, data){
    return this.http[method](url, data ).map((res: Response) => {

      return res.json();

  })
  }

  errorHandler(error) {
    console.log(error);
  

}

}
