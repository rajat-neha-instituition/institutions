import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SuperAdminLoginComponent } from './super-admin-login/super-admin-login.component';
import { ThankyouComponent } from './thankyou/thankyou.component';



const routes : Routes = [
  { path:'',pathMatch:'full',redirectTo: 'login'},
  { path:'login',component:LoginComponent},
  { path:'superAdmin',component:SuperAdminLoginComponent},
  { path:'thankYou',component:ThankyouComponent},


  { path:'superAdminLogin',
	  loadChildren: './super-admin/super-admin.module#SuperAdminModule'},



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
