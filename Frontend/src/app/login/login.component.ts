import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Observable } from 'rxjs';
import { HttpService } from '../http.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  registrationForm : FormGroup;
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
      this.registrationForm = this.formBuilder.group({
        title : ['', [Validators.required]],
        address: ['', [Validators.required]],
        password: ['', [Validators.required]],
        condition: ['', [Validators.required]],
        email: ['', [Validators.required]],
        pincode: ['', [Validators.required]],
        contact: ['', [Validators.required]],

      });

  }


  register() {
    console.log(this.registrationForm.value);
    this.httpService.callHttp('http://localhost:8091/addRequest', 'post', {registrationForm})
            .subscribe((result) => {
                console.log(result);
                this.dataSource.data = result;

            },
            import { Observable } from 'rxjs';
import { HttpService } from '../http.service';
  }


}

